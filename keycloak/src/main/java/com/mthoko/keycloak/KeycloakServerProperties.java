package com.mthoko.keycloak;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "keycloak.server")
@Data
public class KeycloakServerProperties {
    String contextPath = "/auth";
    String realmImportFile = "baeldung-realm.json";
    AdminUser adminUser = new AdminUser();

    // getters and setters

    @Data
    public static class AdminUser {
        String username = "admin";
        String password = "admin";

        // getters and setters        
    }
}